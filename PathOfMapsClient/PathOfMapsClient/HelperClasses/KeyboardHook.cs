﻿using System;
using System.Runtime.InteropServices;

namespace PathOfMapsClient {
    class KeyboardHook {
        [DllImport("user32.dll")]
        static extern IntPtr SetWindowsHookEx(int idHook, LowLevelKeyboardProc callback, IntPtr hInstance, uint threadId);

        [DllImport("user32.dll")]
        static extern bool UnhookWindowsHookEx(IntPtr hInstance);

        [DllImport("user32.dll")]
        static extern IntPtr CallNextHookEx(IntPtr idHook, int nCode, int wParam, IntPtr lParam);

        [DllImport("kernel32.dll")]
        static extern IntPtr LoadLibrary(string lpFileName);

        private const int WH_KEYBOARD_LL = 13;
        private const int WM_KEYDOWN = 0x100;

        private delegate IntPtr LowLevelKeyboardProc(int nCode, IntPtr wParam, IntPtr lParam);
        private LowLevelKeyboardProc _proc;

        private IntPtr hhook = IntPtr.Zero;

        public delegate bool KeyDownHandler(int vkCode);
        public event KeyDownHandler KeyDown;

        public KeyboardHook() {
            this._proc = hookProc;
            SetHook();
        }

        private void SetHook() {
            IntPtr hInstance = LoadLibrary("User32");
            hhook = SetWindowsHookEx(WH_KEYBOARD_LL, _proc, hInstance, 0);
        }

        private IntPtr hookProc(int code, IntPtr wParam, IntPtr lParam) {
            if (code >= 0 && wParam == (IntPtr)WM_KEYDOWN) {
                int vkCode = Marshal.ReadInt32(lParam);
                bool bubble = KeyDown(vkCode);
                if (!bubble) {
                    return new IntPtr(1);
                }
            }

            return CallNextHookEx(hhook, code, (int)wParam, lParam);
        }

        ~KeyboardHook() {
            UnhookWindowsHookEx(hhook);
        }
    }
}
