﻿using System;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Input;

namespace PathOfMapsClient.HelperClasses {
    public class PoMUtils {

        public static bool CtrlPressed() {
            return ((Keyboard.Modifiers & ModifierKeys.Control) > 0);
        }

        public static bool ShiftPressed() {
            return ((Keyboard.Modifiers & ModifierKeys.Shift) > 0);
        }

        public static bool AltPressed() {
            return ((Keyboard.Modifiers & ModifierKeys.Alt) > 0);
        }

        public static string GetNewClipboardContent() {
            WindowsKeySender.PressCtrlC();
            Thread.Sleep(50);
            return Clipboard.GetText();
        }

        /// <summary>
        /// Runs a function in a new STA thread. Some functions can only be used in an STA thread, such as clipboard reading.
        /// </summary>
        /// <param name="d">The function to execute in a new STA thread</param>
        public static void RunOnSTAThread(Action d) {
            Thread t = new Thread(new ThreadStart(d));
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
        }
    }
}
