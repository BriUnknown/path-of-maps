﻿using Newtonsoft.Json;
using PathOfMapsClient.Models;
using System;
using System.ComponentModel;
using System.Net;
using System.Reflection;
using System.Text;

namespace PathOfMapsClient {
    public class PathOfMapsAPI {
        private const string API_URL = "http://pathofmaps.com/logs";
        private const string API_VERSION = "0.1.3";

        public enum Commands {
            [Description(">s")]
            START_MAP,
            [Description(">end")]
            END_MAP,
            [Description(">abort")]
            ABORT_MAP,
            [Description(">reopen")]
            REOPEN_MAP,
            [Description(">d")]
            LOG_DROP,
            [Description(">master")]
            FOUND_MASTER,
            [Description(">craft")]
            ADD_ZANA_MOD
        }

        public PoMAPIResponse FoundMaster(string master) {
            return SendCommand(GetCommandFromEnum(Commands.FOUND_MASTER) + " " + master);
        }

        public PoMAPIResponse AddZanaMod(string zanaMod) {
            return SendCommand(GetCommandFromEnum(Commands.ADD_ZANA_MOD) + " " + zanaMod);
        }

        public PoMAPIResponse StartMap(string mapData) {
            return SendCommand(GetCommandFromEnum(Commands.START_MAP), mapData);
        }

        public PoMAPIResponse LogDrop(string itemData) {
            return SendCommand(GetCommandFromEnum(Commands.LOG_DROP), itemData);
        }

        public PoMAPIResponse EndMap() {
            return SendCommand(GetCommandFromEnum(Commands.END_MAP));
        }

        public PoMAPIResponse AbortMap() {
            return SendCommand(GetCommandFromEnum(Commands.ABORT_MAP));
        }

        public PoMAPIResponse ReOpenMap() {
            return SendCommand(GetCommandFromEnum(Commands.REOPEN_MAP));
        }

        public PoMAPIResponse SendCommandManually(string command) {
            return SendCommand(command);
        }

        private PoMAPIResponse SendCommand(string stringCommand, string itemData = "") {
            //return true;
            using (var wb = new WebClient()) {
                var obj = new {
                    token = MainForm.config.api_token,
                    version = API_VERSION,
                    log = new {
                        command = "2016/04/27 18:57:13 269810609 2e5 [INFO Client 1936] " + MainForm.config.character_name + ": " + stringCommand,
                        map = EncodeTo64(itemData)
                    }
                };

                var json = JsonConvert.SerializeObject(obj);

                wb.Headers.Add(HttpRequestHeader.ContentType, "application/json");
                wb.Headers.Add("X-Requested-With", "XMLHttpRequest");
                try {
                    String data = wb.UploadString(API_URL, "POST", json);
                    PoMAPIResponse response = JsonConvert.DeserializeObject<PoMAPIResponse>(data);
                    return response;
                }
                catch (Exception) {
                    return new PoMAPIResponse();
                }
            }
        }

        private string GetCommandFromEnum(Commands command) {
            FieldInfo fi = command.GetType().GetField(command.ToString());

            DescriptionAttribute attribute = (DescriptionAttribute)fi.GetCustomAttribute(typeof(DescriptionAttribute));
            return attribute.Description;
        }

        private string EncodeTo64(string toEncode) {
            return Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(toEncode));
        }
    }
}
