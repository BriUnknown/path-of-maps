﻿namespace PathOfMapsClient {
    partial class MainForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.lblCurrentMapRunning = new System.Windows.Forms.Label();
            this.lblMapTitle = new System.Windows.Forms.Label();
            this.lblMapIIQ = new System.Windows.Forms.Label();
            this.lblMapIIR = new System.Windows.Forms.Label();
            this.lblMapPackSize = new System.Windows.Forms.Label();
            this.lblMapQuality = new System.Windows.Forms.Label();
            this.lstMapMods = new System.Windows.Forms.ListBox();
            this.lbl3 = new System.Windows.Forms.Label();
            this.lblItemsDropped = new System.Windows.Forms.Label();
            this.lstItemsDropped = new System.Windows.Forms.ListBox();
            this.lblMapMods = new System.Windows.Forms.Label();
            this.lbl1 = new System.Windows.Forms.Label();
            this.lbl2 = new System.Windows.Forms.Label();
            this.picMapIcon = new System.Windows.Forms.PictureBox();
            this.lblStatus = new System.Windows.Forms.Label();
            this.lblZanaMod = new System.Windows.Forms.Label();
            this.lblMaster = new System.Windows.Forms.Label();
            this.lblMapCorrupted = new System.Windows.Forms.Label();
            this.btnSettings = new PathOfMapsClient.FutureButton();
            this.btnFoundMaster = new PathOfMapsClient.FutureButton();
            this.btnAddZanaMod = new PathOfMapsClient.FutureButton();
            this.btnAbortMap = new PathOfMapsClient.FutureButton();
            this.btnReopenMap = new PathOfMapsClient.FutureButton();
            this.btnEndMap = new PathOfMapsClient.FutureButton();
            ((System.ComponentModel.ISupportInitialize)(this.picMapIcon)).BeginInit();
            this.SuspendLayout();
            // 
            // lblCurrentMapRunning
            // 
            this.lblCurrentMapRunning.AutoSize = true;
            this.lblCurrentMapRunning.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(175)))), ((int)(((byte)(244)))));
            this.lblCurrentMapRunning.Location = new System.Drawing.Point(14, 11);
            this.lblCurrentMapRunning.Name = "lblCurrentMapRunning";
            this.lblCurrentMapRunning.Size = new System.Drawing.Size(142, 13);
            this.lblCurrentMapRunning.TabIndex = 6;
            this.lblCurrentMapRunning.Text = "Currently running map:";
            // 
            // lblMapTitle
            // 
            this.lblMapTitle.AutoSize = true;
            this.lblMapTitle.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMapTitle.ForeColor = System.Drawing.Color.White;
            this.lblMapTitle.Location = new System.Drawing.Point(156, 12);
            this.lblMapTitle.MaximumSize = new System.Drawing.Size(260, 26);
            this.lblMapTitle.Name = "lblMapTitle";
            this.lblMapTitle.Size = new System.Drawing.Size(43, 13);
            this.lblMapTitle.TabIndex = 7;
            this.lblMapTitle.Text = "#MAP";
            // 
            // lblMapIIQ
            // 
            this.lblMapIIQ.AutoSize = true;
            this.lblMapIIQ.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(175)))), ((int)(((byte)(244)))));
            this.lblMapIIQ.Location = new System.Drawing.Point(278, 43);
            this.lblMapIIQ.Name = "lblMapIIQ";
            this.lblMapIIQ.Size = new System.Drawing.Size(47, 13);
            this.lblMapIIQ.TabIndex = 8;
            this.lblMapIIQ.Text = "IIQ: %";
            // 
            // lblMapIIR
            // 
            this.lblMapIIR.AutoSize = true;
            this.lblMapIIR.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(175)))), ((int)(((byte)(244)))));
            this.lblMapIIR.Location = new System.Drawing.Point(278, 59);
            this.lblMapIIR.Name = "lblMapIIR";
            this.lblMapIIR.Size = new System.Drawing.Size(46, 13);
            this.lblMapIIR.TabIndex = 9;
            this.lblMapIIR.Text = "IIR: %";
            // 
            // lblMapPackSize
            // 
            this.lblMapPackSize.AutoSize = true;
            this.lblMapPackSize.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(175)))), ((int)(((byte)(244)))));
            this.lblMapPackSize.Location = new System.Drawing.Point(278, 74);
            this.lblMapPackSize.Name = "lblMapPackSize";
            this.lblMapPackSize.Size = new System.Drawing.Size(79, 13);
            this.lblMapPackSize.TabIndex = 10;
            this.lblMapPackSize.Text = "PackSize: %";
            // 
            // lblMapQuality
            // 
            this.lblMapQuality.AutoSize = true;
            this.lblMapQuality.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(175)))), ((int)(((byte)(244)))));
            this.lblMapQuality.Location = new System.Drawing.Point(278, 89);
            this.lblMapQuality.Name = "lblMapQuality";
            this.lblMapQuality.Size = new System.Drawing.Size(68, 13);
            this.lblMapQuality.TabIndex = 11;
            this.lblMapQuality.Text = "Quality: %";
            // 
            // lstMapMods
            // 
            this.lstMapMods.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lstMapMods.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstMapMods.ForeColor = System.Drawing.Color.Yellow;
            this.lstMapMods.FormattingEnabled = true;
            this.lstMapMods.Location = new System.Drawing.Point(17, 165);
            this.lstMapMods.Name = "lstMapMods";
            this.lstMapMods.Size = new System.Drawing.Size(392, 132);
            this.lstMapMods.TabIndex = 12;
            this.lstMapMods.TabStop = false;
            // 
            // lbl3
            // 
            this.lbl3.AutoSize = true;
            this.lbl3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(175)))), ((int)(((byte)(244)))));
            this.lbl3.Location = new System.Drawing.Point(93, 89);
            this.lbl3.Name = "lbl3";
            this.lbl3.Size = new System.Drawing.Size(94, 13);
            this.lbl3.TabIndex = 13;
            this.lbl3.Text = "Corrupted      :";
            // 
            // lblItemsDropped
            // 
            this.lblItemsDropped.AutoSize = true;
            this.lblItemsDropped.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(175)))), ((int)(((byte)(244)))));
            this.lblItemsDropped.Location = new System.Drawing.Point(14, 306);
            this.lblItemsDropped.Name = "lblItemsDropped";
            this.lblItemsDropped.Size = new System.Drawing.Size(98, 13);
            this.lblItemsDropped.TabIndex = 18;
            this.lblItemsDropped.Text = "Items Dropped:";
            // 
            // lstItemsDropped
            // 
            this.lstItemsDropped.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lstItemsDropped.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstItemsDropped.ForeColor = System.Drawing.Color.Yellow;
            this.lstItemsDropped.FormattingEnabled = true;
            this.lstItemsDropped.Location = new System.Drawing.Point(17, 325);
            this.lstItemsDropped.Name = "lstItemsDropped";
            this.lstItemsDropped.Size = new System.Drawing.Size(392, 158);
            this.lstItemsDropped.TabIndex = 19;
            this.lstItemsDropped.TabStop = false;
            // 
            // lblMapMods
            // 
            this.lblMapMods.AutoSize = true;
            this.lblMapMods.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(175)))), ((int)(((byte)(244)))));
            this.lblMapMods.Location = new System.Drawing.Point(14, 146);
            this.lblMapMods.Name = "lblMapMods";
            this.lblMapMods.Size = new System.Drawing.Size(68, 13);
            this.lblMapMods.TabIndex = 24;
            this.lblMapMods.Text = "Map Mods:";
            // 
            // lbl1
            // 
            this.lbl1.AutoSize = true;
            this.lbl1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(175)))), ((int)(((byte)(244)))));
            this.lbl1.Location = new System.Drawing.Point(93, 53);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(94, 13);
            this.lbl1.TabIndex = 29;
            this.lbl1.Text = "Zana mod      :";
            // 
            // lbl2
            // 
            this.lbl2.AutoSize = true;
            this.lbl2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(175)))), ((int)(((byte)(244)))));
            this.lbl2.Location = new System.Drawing.Point(93, 71);
            this.lbl2.Name = "lbl2";
            this.lbl2.Size = new System.Drawing.Size(94, 13);
            this.lbl2.TabIndex = 30;
            this.lbl2.Text = "Master found  :";
            // 
            // picMapIcon
            // 
            this.picMapIcon.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picMapIcon.Location = new System.Drawing.Point(17, 35);
            this.picMapIcon.Name = "picMapIcon";
            this.picMapIcon.Size = new System.Drawing.Size(70, 70);
            this.picMapIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picMapIcon.TabIndex = 5;
            this.picMapIcon.TabStop = false;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.ForeColor = System.Drawing.Color.Red;
            this.lblStatus.Location = new System.Drawing.Point(14, 491);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 13);
            this.lblStatus.TabIndex = 34;
            // 
            // lblZanaMod
            // 
            this.lblZanaMod.AutoSize = true;
            this.lblZanaMod.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(175)))), ((int)(((byte)(244)))));
            this.lblZanaMod.Location = new System.Drawing.Point(184, 53);
            this.lblZanaMod.Name = "lblZanaMod";
            this.lblZanaMod.Size = new System.Drawing.Size(35, 13);
            this.lblZanaMod.TabIndex = 35;
            this.lblZanaMod.Text = "none";
            // 
            // lblMaster
            // 
            this.lblMaster.AutoSize = true;
            this.lblMaster.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(175)))), ((int)(((byte)(244)))));
            this.lblMaster.Location = new System.Drawing.Point(184, 71);
            this.lblMaster.Name = "lblMaster";
            this.lblMaster.Size = new System.Drawing.Size(35, 13);
            this.lblMaster.TabIndex = 36;
            this.lblMaster.Text = "none";
            // 
            // lblMapCorrupted
            // 
            this.lblMapCorrupted.AutoSize = true;
            this.lblMapCorrupted.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(175)))), ((int)(((byte)(244)))));
            this.lblMapCorrupted.Location = new System.Drawing.Point(184, 89);
            this.lblMapCorrupted.Name = "lblMapCorrupted";
            this.lblMapCorrupted.Size = new System.Drawing.Size(21, 13);
            this.lblMapCorrupted.TabIndex = 37;
            this.lblMapCorrupted.Text = "no";
            // 
            // btnSettings
            // 
            this.btnSettings.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSettings.Font = new System.Drawing.Font("Verdana", 8F);
            this.btnSettings.Location = new System.Drawing.Point(334, 11);
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.Size = new System.Drawing.Size(75, 23);
            this.btnSettings.TabIndex = 32;
            this.btnSettings.Text = "Settings";
            this.btnSettings.Click += new System.EventHandler(this.btnSettings_Click);
            // 
            // btnFoundMaster
            // 
            this.btnFoundMaster.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFoundMaster.Font = new System.Drawing.Font("Verdana", 8F);
            this.btnFoundMaster.Location = new System.Drawing.Point(118, 111);
            this.btnFoundMaster.Name = "btnFoundMaster";
            this.btnFoundMaster.Size = new System.Drawing.Size(90, 28);
            this.btnFoundMaster.TabIndex = 28;
            this.btnFoundMaster.TabStop = false;
            this.btnFoundMaster.Text = "Found Master";
            this.btnFoundMaster.Click += new System.EventHandler(this.btnFoundMaster_Click);
            // 
            // btnAddZanaMod
            // 
            this.btnAddZanaMod.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAddZanaMod.Font = new System.Drawing.Font("Verdana", 8F);
            this.btnAddZanaMod.Location = new System.Drawing.Point(17, 111);
            this.btnAddZanaMod.Name = "btnAddZanaMod";
            this.btnAddZanaMod.Size = new System.Drawing.Size(95, 28);
            this.btnAddZanaMod.TabIndex = 27;
            this.btnAddZanaMod.TabStop = false;
            this.btnAddZanaMod.Text = "Add Zana Mod";
            this.btnAddZanaMod.Click += new System.EventHandler(this.btnAddZanaMod_Click);
            // 
            // btnAbortMap
            // 
            this.btnAbortMap.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAbortMap.Font = new System.Drawing.Font("Verdana", 8F);
            this.btnAbortMap.Location = new System.Drawing.Point(281, 111);
            this.btnAbortMap.Name = "btnAbortMap";
            this.btnAbortMap.Size = new System.Drawing.Size(61, 28);
            this.btnAbortMap.TabIndex = 23;
            this.btnAbortMap.TabStop = false;
            this.btnAbortMap.Text = "Abort";
            this.btnAbortMap.Click += new System.EventHandler(this.btnAbortMap_Click);
            // 
            // btnReopenMap
            // 
            this.btnReopenMap.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnReopenMap.Font = new System.Drawing.Font("Verdana", 8F);
            this.btnReopenMap.Location = new System.Drawing.Point(348, 111);
            this.btnReopenMap.Name = "btnReopenMap";
            this.btnReopenMap.Size = new System.Drawing.Size(61, 28);
            this.btnReopenMap.TabIndex = 22;
            this.btnReopenMap.TabStop = false;
            this.btnReopenMap.Text = "Reopen";
            this.btnReopenMap.Click += new System.EventHandler(this.btnReopenMap_Click);
            // 
            // btnEndMap
            // 
            this.btnEndMap.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEndMap.Font = new System.Drawing.Font("Verdana", 8F);
            this.btnEndMap.Location = new System.Drawing.Point(214, 111);
            this.btnEndMap.Name = "btnEndMap";
            this.btnEndMap.Size = new System.Drawing.Size(61, 28);
            this.btnEndMap.TabIndex = 21;
            this.btnEndMap.TabStop = false;
            this.btnEndMap.Text = "End";
            this.btnEndMap.Click += new System.EventHandler(this.btnEndMap_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.ClientSize = new System.Drawing.Size(420, 509);
            this.Controls.Add(this.lblMapCorrupted);
            this.Controls.Add(this.lblMaster);
            this.Controls.Add(this.lblZanaMod);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.btnSettings);
            this.Controls.Add(this.lbl2);
            this.Controls.Add(this.lbl1);
            this.Controls.Add(this.btnFoundMaster);
            this.Controls.Add(this.btnAddZanaMod);
            this.Controls.Add(this.lblMapMods);
            this.Controls.Add(this.btnAbortMap);
            this.Controls.Add(this.btnReopenMap);
            this.Controls.Add(this.btnEndMap);
            this.Controls.Add(this.lstItemsDropped);
            this.Controls.Add(this.lblItemsDropped);
            this.Controls.Add(this.lbl3);
            this.Controls.Add(this.lstMapMods);
            this.Controls.Add(this.lblMapQuality);
            this.Controls.Add(this.lblMapPackSize);
            this.Controls.Add(this.lblMapIIR);
            this.Controls.Add(this.lblMapIIQ);
            this.Controls.Add(this.lblMapTitle);
            this.Controls.Add(this.lblCurrentMapRunning);
            this.Controls.Add(this.picMapIcon);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "Path of Maps";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.picMapIcon)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox picMapIcon;
        private System.Windows.Forms.Label lblCurrentMapRunning;
        private System.Windows.Forms.Label lblMapTitle;
        private System.Windows.Forms.Label lblMapIIQ;
        private System.Windows.Forms.Label lblMapIIR;
        private System.Windows.Forms.Label lblMapPackSize;
        private System.Windows.Forms.Label lblMapQuality;
        private System.Windows.Forms.ListBox lstMapMods;
        private System.Windows.Forms.Label lbl3;
        private System.Windows.Forms.Label lblItemsDropped;
        private System.Windows.Forms.ListBox lstItemsDropped;
        private FutureButton btnEndMap;
        private FutureButton btnReopenMap;
        private FutureButton btnAbortMap;
        private System.Windows.Forms.Label lblMapMods;
        private FutureButton btnAddZanaMod;
        private FutureButton btnFoundMaster;
        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.Label lbl2;
        private FutureButton btnSettings;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label lblZanaMod;
        private System.Windows.Forms.Label lblMaster;
        private System.Windows.Forms.Label lblMapCorrupted;
    }
}

