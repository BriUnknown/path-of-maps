﻿Hi there,

To use this application:

1. Start the application, a window pops up asking you to enter your API key and character name.
2. Enter your API key in the settings window, this is required!
3. Enter your character name using the '+' sign in the settings window next to the characters selection box.
    The currently selected character will be used to send your maps and drops to pathofmaps.com.
4. To log maps, choose hotkeys for starting and logging maps in the settings window by clicking on the text field
    and pressing a key or multiple keys.
5. When running Path of Exile, hover over a map ingame and press the "Start Map" hotkey which you've configured in
    the settings window.
6. When you get a drop, or want to log drops at the end of the map, hover over a drop ingame and press the "Log Drop" 
    hotkey you've configured in the settings window.
7. When you're done with the map, press the "End Map" button or press the "End Map" hotkey!

NOTE: You can also use commands ingame using the chat. Visit http://pathofmaps.com/help for all the commands and their descriptions!

Found an issue/bug, or got an idea? --> https://bitbucket.org/BriUnknown/path-of-maps/issues (no need to register!)
The latest version will be posted   --> https://bitbucket.org/BriUnknown/path-of-maps/downloads
										(NOTE: the application checks for updates each time it starts!)

As always, visit http://pathofmaps.com for more info, statistics and your personal mapping log!

Have fun!