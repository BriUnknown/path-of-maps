﻿using System;
using System.Windows.Forms;

namespace PathOfMapsClient {
    public partial class ZanaModForm : Form {
        public ZanaModForm() {
            InitializeComponent();
        }

        public enum ZanaMods {
            NO_MOD,
            ONSLAUGHT,
            RAMPAGE,
            ANARCHY,
            BLOODLINES,
            TORMENT,
            WARBANDS,
            BEYOND,
            AMBUSH,
            TEMPEST,
            DOMINATION,
            NEMESIS
        }

        public ZanaMods ZanaMod { get; set; }

        private void btnZanaModPicked(object sender, EventArgs e) {
            switch (((Control)sender).Name) {
                case "btnRampage":
                    ZanaMod = ZanaMods.RAMPAGE;
                    break;
                case "btnBloodlines":
                    ZanaMod = ZanaMods.BLOODLINES;
                    break;
                case "btnTorment":
                    ZanaMod = ZanaMods.TORMENT;
                    break;
                case "btnTempest":
                    ZanaMod = ZanaMods.TEMPEST;
                    break;
                case "btnDomination":
                    ZanaMod = ZanaMods.DOMINATION;
                    break;
                case "btnAmbush":
                    ZanaMod = ZanaMods.AMBUSH;
                    break;
                case "btnNemesis":
                    ZanaMod = ZanaMods.NEMESIS;
                    break;
                case "btnNoMod":
                default:
                    ZanaMod = ZanaMods.NO_MOD;
                    break;
            }
            this.Close();
        }
    }
}
