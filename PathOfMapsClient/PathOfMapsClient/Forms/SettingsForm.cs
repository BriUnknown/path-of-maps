﻿using Microsoft.VisualBasic;
using PathOfMapsClient.HelperClasses;
using PathOfMapsClient.Models;
using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace PathOfMapsClient.Forms {
    public partial class SettingsForm : Form {
        public SettingsForm() {
            InitializeComponent();
            this.Shown += SettingsForm_Shown;
        }

        private BindingSource bs = new BindingSource();

        private void SettingsForm_Shown(object sender, System.EventArgs e) {
            if (MainForm.config.start_map_key.HasValue) {
                Keys startNewMapKey = MainForm.config.start_map_key.Value;
                if (MainForm.config.start_map_key_alt) {
                    startNewMapKey |= Keys.Alt;
                }
                if (MainForm.config.start_map_key_ctrl) {
                    startNewMapKey |= Keys.Control;
                }
                if (MainForm.config.start_map_key_shift) {
                    startNewMapKey |= Keys.Shift;
                }

                txtStartNewMapKey.Text = startNewMapKey.ToString();
            }

            if (MainForm.config.end_map_key.HasValue) {
                Keys endMapKey = MainForm.config.end_map_key.Value;
                if (MainForm.config.end_map_key_alt) {
                    endMapKey |= Keys.Alt;
                }
                if (MainForm.config.end_map_key_ctrl) {
                    endMapKey |= Keys.Control;
                }
                if (MainForm.config.end_map_key_shift) {
                    endMapKey |= Keys.Shift;
                }

                txtEndMapKey.Text = endMapKey.ToString();
            }

            if (MainForm.config.log_drop_key.HasValue) {
                Keys logDropKey = MainForm.config.log_drop_key.Value;
                if (MainForm.config.log_drop_key_alt) {
                    logDropKey |= Keys.Alt;
                }
                if (MainForm.config.log_drop_key_ctrl) {
                    logDropKey |= Keys.Control;
                }
                if (MainForm.config.log_drop_key_shift) {
                    logDropKey |= Keys.Shift;
                }

                txtLogDropKey.Text = logDropKey.ToString();
            }

            chkAlwaysTopmost.Checked = MainForm.config.always_topmost;
            txtApiToken.Text = MainForm.config.api_token;
            txtClientTxtLocation.Text = MainForm.config.client_txt_location;

            bs.DataSource = MainForm.config.character_names;
            cbbCharacterNames.DataSource = bs;
            cbbCharacterNames.SelectedItem = MainForm.config.character_name;

            if (MainForm.config.log_read_interval == 0) {
                MainForm.config.log_read_interval = 2000;
            }
            numericUpDown1.Value = MainForm.config.log_read_interval;

            this.ActiveControl = label1;
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData) {
            if (txtStartNewMapKey.Focused) {
                txtStartNewMapKey.Text = keyData.ToString();
                MainForm.config.start_map_key = (keyData & ~(Keys.Control | Keys.Alt | Keys.Shift));
                MainForm.config.start_map_key_alt = ((keyData & Keys.Alt) == Keys.Alt);
                MainForm.config.start_map_key_ctrl = ((keyData & Keys.Control) == Keys.Control);
                MainForm.config.start_map_key_shift = ((keyData & Keys.Shift) == Keys.Shift);
            }
            else if (txtLogDropKey.Focused) {
                txtLogDropKey.Text = keyData.ToString();
                MainForm.config.log_drop_key = (keyData & ~(Keys.Control | Keys.Alt | Keys.Shift));
                MainForm.config.log_drop_key_alt = ((keyData & Keys.Alt) == Keys.Alt);
                MainForm.config.log_drop_key_ctrl = ((keyData & Keys.Control) == Keys.Control);
                MainForm.config.log_drop_key_shift = ((keyData & Keys.Shift) == Keys.Shift);
            }
            else if (txtEndMapKey.Focused) {
                txtEndMapKey.Text = keyData.ToString();
                MainForm.config.end_map_key = (keyData & ~(Keys.Control | Keys.Alt | Keys.Shift));
                MainForm.config.end_map_key_alt = ((keyData & Keys.Alt) == Keys.Alt);
                MainForm.config.end_map_key_ctrl = ((keyData & Keys.Control) == Keys.Control);
                MainForm.config.end_map_key_shift = ((keyData & Keys.Shift) == Keys.Shift);
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void txtApiToken_TextChanged(object sender, System.EventArgs e) {
            MainForm.config.api_token = txtApiToken.Text;
        }

        private void btnToggleAPITokenDisplay_Click(object sender, System.EventArgs e) {
            if (txtApiToken.PasswordChar == '*') {
                txtApiToken.PasswordChar = new char();
            }
            else {
                txtApiToken.PasswordChar = '*';
            }
        }

        private void btnDeleteCharacter_Click(object sender, System.EventArgs e) {
            if (MessageBox.Show("Are you sure you want to delete this character?",
                "Delete character", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) {
                bs.Remove(cbbCharacterNames.SelectedItem.ToString());
            }
        }

        private void btnAddCharacter_Click(object sender, System.EventArgs e) {
            string charName = Interaction.InputBox("Enter character name:", "Enter character name");
            if (!string.IsNullOrWhiteSpace(charName) && !string.IsNullOrEmpty(charName)) {
                bs.Add(charName);
                MainForm.config.character_name = cbbCharacterNames.SelectedItem.ToString();
            }
        }

        private void linkNoApiToken_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            Process.Start("http://pathofmaps.com/users/sign_up");
        }

        private void chkAlwaysTopmost_CheckedChanged(object sender, System.EventArgs e) {
            MainForm.config.always_topmost = chkAlwaysTopmost.Checked;
        }

        private async void btnCheckUpdates_Click(object sender, System.EventArgs e) {
            UpdateObj update = await Updater.CheckForUpdates();
            if (update.releases != null && update.releases.Count > 0) {
                Release release = update.releases[update.releases.Count - 1];
                DialogResult result = MessageBox.Show("There is an update available! Version " + release.version +
                        " can be downloaded.\n\nWould you like to visit the website to download the latest version?",
                        "Update available", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (result == DialogResult.Yes) {
                    Process.Start(release.download_url);
                }
            }
            else if (update.releases != null && update.releases.Count == 0) {
                MessageBox.Show("No update available, you have the latest version!",
                                        "You have the latest version!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else {
                MessageBox.Show("Checking for updates failed. Please check your internet connection.",
                                    "Checking for updates failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtClientTxtLocation_TextChanged(object sender, System.EventArgs e) {
            MainForm.config.client_txt_location = txtClientTxtLocation.Text;
        }

        private void btnClientTxtLocation_Click(object sender, System.EventArgs e) {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.DefaultExt = "*.txt";
            dialog.FileName = "Client.txt";
            dialog.Filter = "Text Files (.txt)|*.txt";
            dialog.Title = "Please select your Path of Exile's Client.txt file";
            if (dialog.ShowDialog() == DialogResult.OK) {
                if (dialog.FileName.ToLower().EndsWith("client.txt")) {
                    txtClientTxtLocation.Text = dialog.FileName;
                }
                else {
                    MessageBox.Show("You have not selected the Client.txt file from Path of Exile, please try again!",
                        "Try again", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void numericUpDown1_ValueChanged(object sender, System.EventArgs e) {
            MainForm.config.log_read_interval = Convert.ToInt32(numericUpDown1.Value);
        }

        private void cbbCharacterNames_SelectionChangeCommitted(object sender, EventArgs e) {
            MainForm.config.character_name = cbbCharacterNames.SelectedItem.ToString();
        }
    }
}
