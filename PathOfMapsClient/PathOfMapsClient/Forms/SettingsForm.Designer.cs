﻿namespace PathOfMapsClient.Forms {
    partial class SettingsForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.txtStartNewMapKey = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtLogDropKey = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblCharacterName = new System.Windows.Forms.Label();
            this.lblApiToken = new System.Windows.Forms.Label();
            this.txtApiToken = new System.Windows.Forms.TextBox();
            this.cbbCharacterNames = new System.Windows.Forms.ComboBox();
            this.linkNoApiToken = new System.Windows.Forms.LinkLabel();
            this.label4 = new System.Windows.Forms.Label();
            this.chkAlwaysTopmost = new System.Windows.Forms.CheckBox();
            this.btnDeleteCharacter = new PathOfMapsClient.FutureButton();
            this.btnAddCharacter = new PathOfMapsClient.FutureButton();
            this.btnToggleAPITokenDisplay = new PathOfMapsClient.FutureButton();
            this.btnCheckUpdates = new PathOfMapsClient.FutureButton();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtClientTxtLocation = new System.Windows.Forms.TextBox();
            this.btnClientTxtLocation = new PathOfMapsClient.FutureButton();
            this.label7 = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.txtEndMapKey = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtStartNewMapKey
            // 
            this.txtStartNewMapKey.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.txtStartNewMapKey.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtStartNewMapKey.ForeColor = System.Drawing.Color.Yellow;
            this.txtStartNewMapKey.Location = new System.Drawing.Point(163, 220);
            this.txtStartNewMapKey.Name = "txtStartNewMapKey";
            this.txtStartNewMapKey.ReadOnly = true;
            this.txtStartNewMapKey.Size = new System.Drawing.Size(253, 21);
            this.txtStartNewMapKey.TabIndex = 2;
            this.txtStartNewMapKey.Text = "Press to select key..";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 222);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(130, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Key to start new map";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 276);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Key to log drop";
            // 
            // txtLogDropKey
            // 
            this.txtLogDropKey.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.txtLogDropKey.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLogDropKey.ForeColor = System.Drawing.Color.Yellow;
            this.txtLogDropKey.Location = new System.Drawing.Point(163, 274);
            this.txtLogDropKey.Name = "txtLogDropKey";
            this.txtLogDropKey.ReadOnly = true;
            this.txtLogDropKey.Size = new System.Drawing.Size(253, 21);
            this.txtLogDropKey.TabIndex = 4;
            this.txtLogDropKey.Text = "Press to select key..";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(36, 194);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(353, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "---Click on a text field below to change the hotkey---";
            // 
            // lblCharacterName
            // 
            this.lblCharacterName.AutoSize = true;
            this.lblCharacterName.Font = new System.Drawing.Font("Verdana", 8.25F);
            this.lblCharacterName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(175)))), ((int)(((byte)(244)))));
            this.lblCharacterName.Location = new System.Drawing.Point(12, 58);
            this.lblCharacterName.Name = "lblCharacterName";
            this.lblCharacterName.Size = new System.Drawing.Size(109, 13);
            this.lblCharacterName.TabIndex = 30;
            this.lblCharacterName.Text = "Current character";
            // 
            // lblApiToken
            // 
            this.lblApiToken.AutoSize = true;
            this.lblApiToken.Font = new System.Drawing.Font("Verdana", 8.25F);
            this.lblApiToken.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(175)))), ((int)(((byte)(244)))));
            this.lblApiToken.Location = new System.Drawing.Point(12, 28);
            this.lblApiToken.Name = "lblApiToken";
            this.lblApiToken.Size = new System.Drawing.Size(65, 13);
            this.lblApiToken.TabIndex = 29;
            this.lblApiToken.Text = "API Token";
            // 
            // txtApiToken
            // 
            this.txtApiToken.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.txtApiToken.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtApiToken.ForeColor = System.Drawing.Color.Yellow;
            this.txtApiToken.Location = new System.Drawing.Point(163, 26);
            this.txtApiToken.Name = "txtApiToken";
            this.txtApiToken.PasswordChar = '*';
            this.txtApiToken.Size = new System.Drawing.Size(253, 21);
            this.txtApiToken.TabIndex = 27;
            this.txtApiToken.TextChanged += new System.EventHandler(this.txtApiToken_TextChanged);
            // 
            // cbbCharacterNames
            // 
            this.cbbCharacterNames.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.cbbCharacterNames.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbCharacterNames.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbbCharacterNames.ForeColor = System.Drawing.Color.Yellow;
            this.cbbCharacterNames.FormattingEnabled = true;
            this.cbbCharacterNames.Location = new System.Drawing.Point(163, 55);
            this.cbbCharacterNames.Name = "cbbCharacterNames";
            this.cbbCharacterNames.Size = new System.Drawing.Size(191, 21);
            this.cbbCharacterNames.TabIndex = 33;
            this.cbbCharacterNames.SelectionChangeCommitted += new System.EventHandler(this.cbbCharacterNames_SelectionChangeCommitted);
            // 
            // linkNoApiToken
            // 
            this.linkNoApiToken.AutoSize = true;
            this.linkNoApiToken.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(175)))), ((int)(((byte)(244)))));
            this.linkNoApiToken.Location = new System.Drawing.Point(160, 7);
            this.linkNoApiToken.Name = "linkNoApiToken";
            this.linkNoApiToken.Size = new System.Drawing.Size(219, 13);
            this.linkNoApiToken.TabIndex = 37;
            this.linkNoApiToken.TabStop = true;
            this.linkNoApiToken.Text = "Don\'t have an API token? Click here!";
            this.linkNoApiToken.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkNoApiToken_LinkClicked);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 8.25F);
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(175)))), ((int)(((byte)(244)))));
            this.label4.Location = new System.Drawing.Point(12, 86);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 13);
            this.label4.TabIndex = 38;
            this.label4.Text = "Always topmost";
            // 
            // chkAlwaysTopmost
            // 
            this.chkAlwaysTopmost.AutoSize = true;
            this.chkAlwaysTopmost.Location = new System.Drawing.Point(163, 86);
            this.chkAlwaysTopmost.Name = "chkAlwaysTopmost";
            this.chkAlwaysTopmost.Size = new System.Drawing.Size(15, 14);
            this.chkAlwaysTopmost.TabIndex = 39;
            this.chkAlwaysTopmost.UseVisualStyleBackColor = true;
            this.chkAlwaysTopmost.CheckedChanged += new System.EventHandler(this.chkAlwaysTopmost_CheckedChanged);
            // 
            // btnDeleteCharacter
            // 
            this.btnDeleteCharacter.Font = new System.Drawing.Font("Verdana", 8F);
            this.btnDeleteCharacter.Location = new System.Drawing.Point(360, 55);
            this.btnDeleteCharacter.Name = "btnDeleteCharacter";
            this.btnDeleteCharacter.Size = new System.Drawing.Size(26, 21);
            this.btnDeleteCharacter.TabIndex = 35;
            this.btnDeleteCharacter.Text = "X";
            this.btnDeleteCharacter.Click += new System.EventHandler(this.btnDeleteCharacter_Click);
            // 
            // btnAddCharacter
            // 
            this.btnAddCharacter.Font = new System.Drawing.Font("Verdana", 8F);
            this.btnAddCharacter.Location = new System.Drawing.Point(390, 55);
            this.btnAddCharacter.Name = "btnAddCharacter";
            this.btnAddCharacter.Size = new System.Drawing.Size(26, 21);
            this.btnAddCharacter.TabIndex = 34;
            this.btnAddCharacter.Text = "+";
            this.btnAddCharacter.Click += new System.EventHandler(this.btnAddCharacter_Click);
            // 
            // btnToggleAPITokenDisplay
            // 
            this.btnToggleAPITokenDisplay.Font = new System.Drawing.Font("Verdana", 8F);
            this.btnToggleAPITokenDisplay.Location = new System.Drawing.Point(401, 7);
            this.btnToggleAPITokenDisplay.Name = "btnToggleAPITokenDisplay";
            this.btnToggleAPITokenDisplay.Size = new System.Drawing.Size(15, 16);
            this.btnToggleAPITokenDisplay.TabIndex = 32;
            this.btnToggleAPITokenDisplay.Text = "?";
            this.btnToggleAPITokenDisplay.Click += new System.EventHandler(this.btnToggleAPITokenDisplay_Click);
            // 
            // btnCheckUpdates
            // 
            this.btnCheckUpdates.Font = new System.Drawing.Font("Verdana", 8F);
            this.btnCheckUpdates.Location = new System.Drawing.Point(163, 166);
            this.btnCheckUpdates.Name = "btnCheckUpdates";
            this.btnCheckUpdates.Size = new System.Drawing.Size(145, 23);
            this.btnCheckUpdates.TabIndex = 40;
            this.btnCheckUpdates.Text = "Check for updates";
            this.btnCheckUpdates.Visible = false;
            this.btnCheckUpdates.Click += new System.EventHandler(this.btnCheckUpdates_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Verdana", 8.25F);
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(175)))), ((int)(((byte)(244)))));
            this.label5.Location = new System.Drawing.Point(12, 173);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 13);
            this.label5.TabIndex = 41;
            this.label5.Text = "Updates";
            this.label5.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Verdana", 8.25F);
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(175)))), ((int)(((byte)(244)))));
            this.label6.Location = new System.Drawing.Point(11, 111);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(110, 13);
            this.label6.TabIndex = 42;
            this.label6.Text = "Client.txt Location";
            // 
            // txtClientTxtLocation
            // 
            this.txtClientTxtLocation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.txtClientTxtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtClientTxtLocation.ForeColor = System.Drawing.Color.Yellow;
            this.txtClientTxtLocation.Location = new System.Drawing.Point(163, 108);
            this.txtClientTxtLocation.Name = "txtClientTxtLocation";
            this.txtClientTxtLocation.Size = new System.Drawing.Size(223, 21);
            this.txtClientTxtLocation.TabIndex = 43;
            this.txtClientTxtLocation.TextChanged += new System.EventHandler(this.txtClientTxtLocation_TextChanged);
            // 
            // btnClientTxtLocation
            // 
            this.btnClientTxtLocation.Font = new System.Drawing.Font("Verdana", 8F);
            this.btnClientTxtLocation.Location = new System.Drawing.Point(390, 108);
            this.btnClientTxtLocation.Name = "btnClientTxtLocation";
            this.btnClientTxtLocation.Size = new System.Drawing.Size(26, 21);
            this.btnClientTxtLocation.TabIndex = 44;
            this.btnClientTxtLocation.Text = "...";
            this.btnClientTxtLocation.Click += new System.EventHandler(this.btnClientTxtLocation_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Verdana", 8.25F);
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(175)))), ((int)(((byte)(244)))));
            this.label7.Location = new System.Drawing.Point(11, 139);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(140, 13);
            this.label7.TabIndex = 45;
            this.label7.Text = "Log Read Interval (ms)";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numericUpDown1.Location = new System.Drawing.Point(163, 137);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(120, 21);
            this.numericUpDown1.TabIndex = 46;
            this.numericUpDown1.Value = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 249);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(98, 13);
            this.label8.TabIndex = 48;
            this.label8.Text = "Key to end map";
            // 
            // txtEndMapKey
            // 
            this.txtEndMapKey.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.txtEndMapKey.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEndMapKey.ForeColor = System.Drawing.Color.Yellow;
            this.txtEndMapKey.Location = new System.Drawing.Point(163, 247);
            this.txtEndMapKey.Name = "txtEndMapKey";
            this.txtEndMapKey.ReadOnly = true;
            this.txtEndMapKey.Size = new System.Drawing.Size(253, 21);
            this.txtEndMapKey.TabIndex = 47;
            this.txtEndMapKey.Text = "Press to select key..";
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.ClientSize = new System.Drawing.Size(428, 308);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtEndMapKey);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btnClientTxtLocation);
            this.Controls.Add(this.txtClientTxtLocation);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnCheckUpdates);
            this.Controls.Add(this.chkAlwaysTopmost);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.linkNoApiToken);
            this.Controls.Add(this.btnDeleteCharacter);
            this.Controls.Add(this.btnAddCharacter);
            this.Controls.Add(this.cbbCharacterNames);
            this.Controls.Add(this.btnToggleAPITokenDisplay);
            this.Controls.Add(this.lblCharacterName);
            this.Controls.Add(this.lblApiToken);
            this.Controls.Add(this.txtApiToken);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtLogDropKey);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtStartNewMapKey);
            this.Font = new System.Drawing.Font("Verdana", 8.25F);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(175)))), ((int)(((byte)(244)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.KeyPreview = true;
            this.Name = "SettingsForm";
            this.Text = "Path of Maps Settings";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox txtStartNewMapKey;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtLogDropKey;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblCharacterName;
        private System.Windows.Forms.Label lblApiToken;
        private System.Windows.Forms.TextBox txtApiToken;
        private FutureButton btnToggleAPITokenDisplay;
        private System.Windows.Forms.ComboBox cbbCharacterNames;
        private FutureButton btnAddCharacter;
        private FutureButton btnDeleteCharacter;
        private System.Windows.Forms.LinkLabel linkNoApiToken;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chkAlwaysTopmost;
        private FutureButton btnCheckUpdates;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtClientTxtLocation;
        private FutureButton btnClientTxtLocation;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtEndMapKey;
    }
}