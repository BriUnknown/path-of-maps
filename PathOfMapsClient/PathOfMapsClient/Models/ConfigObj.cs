﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace PathOfMapsClient.HelperClasses {
    public class ConfigObj {
        public string api_token { get; set; }
        public string character_name { get; set; }
        public List<string> character_names { get; set; }
        public Keys? start_map_key { get; set; }
        public bool start_map_key_alt { get; set; }
        public bool start_map_key_ctrl { get; set; }
        public bool start_map_key_shift { get; set; }
        public Keys? end_map_key { get; set; }
        public bool end_map_key_alt { get; set; }
        public bool end_map_key_ctrl { get; set; }
        public bool end_map_key_shift { get; set; }
        public Keys? log_drop_key { get; set; }
        public bool log_drop_key_alt { get; set; }
        public bool log_drop_key_ctrl { get; set; }
        public bool log_drop_key_shift { get; set; }
        public bool always_topmost { get; set; }
        public string client_txt_location { get; set; }
        public int log_read_interval { get; set; }

        public ConfigObj() {
            this.character_names = new List<string>();
        }
    }
}
