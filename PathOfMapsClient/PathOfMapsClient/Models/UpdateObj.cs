﻿using System.Collections.Generic;

namespace PathOfMapsClient.Models {
    public class UpdateObj {
        public List<Release> releases { get; set; }
    }

    public class Release {
        public int id { get; set; }
        public string title { get; set; }
        public string changelog { get; set; }
        public string version { get; set; }
        public string download_url { get; set; }
        public string source_url { get; set; }
        public string created_at { get; set; }
        public string pom_path { get; set; }
    }
}
